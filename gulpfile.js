const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");
const concat = require("gulp-concat");
const autoprefixer = require("gulp-autoprefixer");
const cssmin = require("gulp-cssmin");
const rename = require("gulp-rename");
const minify = require("gulp-minify");
const deleteFiles = require("delete");
const sass = require("gulp-sass")(require("sass"));
const { watch } = require("gulp");

gulp.task("html-minify", function () {
  return gulp
    .src("src/html/index.html")
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("build"));
});

gulp.task("buildJs", function () {
  return gulp.src("src/js/*.js").pipe(minify()).pipe(gulp.dest("build"));
});

gulp.task("deleteFiles", function () {
  return deleteFiles("build/");
});

gulp.task("build-css", function () {
  return (
    gulp
      .src("src/scss/style.scss")
      .pipe(
        autoprefixer({
          cascade: false,
        })
      )
      .pipe(sass().on("error", sass.logError))
      // .pipe(concat("styles.css"))
      .pipe(cssmin())
      .pipe(rename({ suffix: ".min" }))
      .pipe(gulp.dest("build/css"))
  );
});

gulp.task("buildImg", function () {
  return gulp.src("src/img/*.png").pipe(gulp.dest("build/img/"));
});

gulp.task(
  "build",
  gulp.series(
    "deleteFiles",
    gulp.parallel("html-minify", "buildJs", "build-css", "buildImg")
  )
);

gulp.watch(
  ["src/scss/**/**/*.scss", "src/html/*.html", "src/js/*.js"],
  gulp.series("build")
);


